/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Organization.Organization.Type;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public class OrganizationDirectory {
    
    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    
    public Organization createOrganization(Type type){
        Organization organization = null;
        if (type.getValue().equals(Type.Doctor.getValue())){
            organization = new DoctorOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.Lab.getValue())){
            organization = new LabOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.Asylum.getValue())){
            organization = new AsylumOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.HighSchool.getValue())){
            organization = new HighSchoolOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.School.getValue())){
            organization = new SchoolOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.MarketOrientedSkill.getValue())){
            organization = new MarketOrientedSkillOragnization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.MicroSmallEmployment.getValue())){
            organization = new MicroSmallMediumEnterpriseOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.WageEmployment.getValue())){
            organization = new WageEmploymentOragnization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.Identification.getValue())){
            organization = new IdentificationOrganisation();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.Interviewer.getValue())){
            organization = new InterviewerOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.EmploymentInterviewer.getValue())){
            organization = new EmploymentInterviewer();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.SkilledEmployment.getValue())){
            organization = new SkilledEmploymentOrganization();
            organizationList.add(organization);
        }
        
        return organization;
    }
}

