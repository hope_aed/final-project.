/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Refugee;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author don
 */
public class RefugeeDirectory {
    
    private ArrayList<Refugee> refugeeDirectoryList;
    
    public RefugeeDirectory(){
        refugeeDirectoryList = new ArrayList();
    }

    public ArrayList<Refugee> getRefugeeDirectoryList() {
        return refugeeDirectoryList;
    }

    public void deleteRefugeeDetails(Refugee refugee){
        refugeeDirectoryList.remove(refugee);
        
    }
    
    public Refugee createRefugee(String firstName, String lastName, String gender, String bloodgroup,
        String refugee_dob, String homeCountry, String qualification, String workExperience, String reason, String uniqueId){


        Refugee refugeeDetails = new Refugee();
        refugeeDetails.setFirstName(firstName);
        refugeeDetails.setLastName(lastName);
        refugeeDetails.setGender(gender);
        refugeeDetails.setBloodGroup(bloodgroup);
        refugeeDetails.setRefugee_dob(refugee_dob);
        refugeeDetails.setHomeCountry(homeCountry);
        refugeeDetails.setQualification(qualification);
        refugeeDetails.setWorkExperience(workExperience);
        refugeeDetails.setReasonForDisplacement(reason);
        refugeeDetails.setUniqueId(uniqueId);
        refugeeDirectoryList.add(refugeeDetails);
        return refugeeDetails;
    }
        
}



