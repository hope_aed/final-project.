/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Refugee;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author don
 */
public class DetailsDirectory {
        private ArrayList<Details> detailsDirectoryList;

    public DetailsDirectory(){
        detailsDirectoryList = new ArrayList();
    }
        
    public ArrayList<Details> getDetailsDirectoryList() {
        return detailsDirectoryList;
    }

    public void setDetailsDirectoryList(ArrayList<Details> detailsDirectoryList) {
        this.detailsDirectoryList = detailsDirectoryList;
    }

    public Details createDeatilsDir(String uniqueId, String asylum){
        Details details = new Details();
        details.setUniqueId(uniqueId);
        details.setAsylum(asylum);
        detailsDirectoryList.add(details);
        return details;
    
    }
     public Details createDeatilsDirEmp(String uniqueId, String asylum,String employment){
        Details details = new Details();
        details.setUniqueId(uniqueId);
        details.setAsylum(asylum);
        details.setEmployment(employment);
        detailsDirectoryList.add(details);
        return details;
    
    }
}
    
