/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Refugee;

import java.util.Date;

/**
 *
 * @author don
 */
public class Details {
    private String uniqueId;
    private String asylum;
    private String Health;
    private String School;
    private String employment;

    public String getHealth() {
        return Health;
    }

    public void setHealth(String Health) {
        this.Health = Health;
    }

    public String getSchool() {
        return School;
    }

    public void setSchool(String School) {
        this.School = School;
    }

    public String getEmployment() {
        return employment;
    }

    public void setEmployment(String employment) {
        this.employment = employment;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getAsylum() {
        return asylum;
    }

    public void setAsylum(String asylum) {
        this.asylum = asylum;
    }
     @Override
    public String toString(){
        return uniqueId;
    }

}

