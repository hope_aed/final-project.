/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author don
 */
public class HousingWorkRequest extends WorkRequest{
    
    private String houseAssignment;

    
    public String getHouseAssignment() {
        return houseAssignment;
    }

    public void setHouseAssignment(String houseAssignment) {
        this.houseAssignment = houseAssignment;
    }

    
    
}
